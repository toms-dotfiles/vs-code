*Table of Contents*

[[_TOC_]]

---

# Setup VS Code for Development

Developers are free to use any IDE that they choose (_a text editor is not an IDE_). However, VS Code is highly recommended because it is fast, extensible, self updating, and multi-platform compatible.

***NOTE:*** VS Code uses commented JSON for their settings which might break some linters.

## Download VS Code

For the latest user versions of VS Code, download using this link: https://code.visualstudio.com/updates  

***NOTE:*** For a version which is almost identical but without any telemetry, try [vscodium](https://github.com/VSCodium/vscodium)

# Extensions for VS Code

To get extensions installed for VS Code, open the extension marketplace on the left sidebar. Then search for the desired extensions using the `Id`s below.  

> There are times that editing VS Code's `settings.json` is recommended in order to get better functionality from the extensions.  
> Go to https://vscode.readthedocs.io/en/latest/getstarted/settings/ to see how to edit VS Code's user `settings.json` for your specific platform.

## Cloud Providers

### Azure Cloud

1. Azure Account
    - Id: ms-vscode.azure-account
    - Description: A common Sign-In and Subscription management extension for VS Code.
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-vscode.azure-account

1. Azure Pipelines
    - Id: ms-azure-devops.azure-pipelines
    - Description: Syntax highlighting, IntelliSense, and more for Azure Pipelines YAML
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-azure-devops.azure-pipelines

## Containers

1. Docker
    - Id: ms-azuretools.vscode-docker
    - Description: Makes it easy to create, manage, and debug containerized applications.
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker

1. Kubernetes
    - Id: ms-kubernetes-tools.vscode-kubernetes-tools
    - Description: Develop, deploy and debug Kubernetes applications
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools
    
    > Edit VS Code's `settings.json` to include the following, this includes some optional settings as examples:

    ```json
    {
      // ms-kubernetes-tools.vscode-kubernetes-tools
      // NOTE: replace usernames as needed
      "vs-kubernetes": {
        "vs-kubernetes.helm-path.windows": "C:\\Users\\us58958\\.vs-kubernetes\\tools\\helm\\windows-amd64\\helm.exe",
        "vs-kubernetes.draft-path.windows": "C:\\Users\\us58958\\.vs-kubernetes\\tools\\draft\\windows-amd64\\draft.exe",
        "vs-kubernetes.minikube-path.windows": "C:\\Users\\us58958\\.vs-kubernetes\\tools\\minikube\\windows-amd64\\minikube.exe",
        "vs-kubernetes.helm-path.linux": "/home/tom/.vs-kubernetes/tools/helm/linux-amd64/helm",
        "vs-kubernetes.draft-path.linux": "/home/tom/.vs-kubernetes/tools/draft/linux-amd64/draft",
        "vs-kubernetes.minikube-path.linux": "/home/tom/.vs-kubernetes/tools/minikube/linux-amd64/minikube"
      }
    }
    ```

1. vscode-base64
    - Id: adamhartford.vscode-base64
    - Description: Base64 encode/decode the current selections.
    - Publisher: Adam Hartford
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=adamhartford.vscode-base64

## Emacs Emulation

1. Emacs
    - Id: vscodeemacs.emacs
    - Description: Emacs emulation for Visual Studio Code
    - Publisher: VSCodeEmacs
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscodeemacs.emacs

1. Org Mode
    - Id: vscode-org-mode.org-mode
    - Description: Emacs Org mode support for VSCode
    - Publisher: vscode-org-mode
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscode-org-mode.org-mode

## Languages

### C#

1. C#
    - Id: ms-dotnettools.csharp
    - Description: C# for Visual Studio Code (powered by OmniSharp).
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp

### Golang

1. Go
     - Id: golang.go  
     - Description: Rich Go language support for Visual Studio Code  
     - Publisher: Go Team at Google  
     - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=golang.Go
    
    > Edit VS Code's `settings.json` to include the following, this includes some optional settings as examples:  

    ```json
    {
      // golang.go
      "go.autocompleteUnimportedPackages": true,
      "go.useLanguageServer": true
    }
    ```
        
2. Go Critic
    - Id: neverik.go-critic
    - Description: Integration for the go-critic golang linter.
    - Publisher: neverik
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=neverik.go-critic

### Java

1. Debugger for Java
    - Id: vscjava.vscode-java-debug
    - Description: A lightweight Java debugger for Visual Studio Code
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-debug

1. Java Dependency Viewer
    - Id: vscjava.vscode-java-dependency
    - Description: Manage Java Dependencies in VSCode
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-dependency

1. Java Extension Pack
    - Id: vscjava.vscode-java-pack
    - Description: Popular extensions for Java development and more.
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack

1. Java Test Runner
    - Id: vscjava.vscode-java-test
    - Description: Run and debug JUnit or TestNG test cases
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-test

1. Language Support for Java(TM) by Red Hat
    - Id: redhat.java
    - Description: Java Linting, Intellisense, formatting, refactoring, Maven/Gradle support and more...
    - Publisher: Red Hat
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=redhat.java

1. Maven for Java
    - Id: vscjava.vscode-maven
    - Description: Manage Maven projects, execute goals, generate project from archetype, improve user experience for Java developers.
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-maven

### JSON

1. Fix JSON
    - Id: oliversturm.fix-json
    - Description: Fix JSON content using jsonic
    - Publisher: Oliver Sturm
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=oliversturm.fix-json

1. JSON Tools
    - Id: eriklynd.json-tools
    - Description: Tools for manipulating JSON
    - Publisher: Erik Lynd
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=eriklynd.json-tools

1. Sort JSON objects
    - Id: richie5um2.vscode-sort-json
    - Description: Sorts the keys within JSON objects
    - Publisher: richie5um2
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=richie5um2.vscode-sort-json

### Markdown

1. CSV to Markdown Table Converter
    - Id: marchiore.csvtomarkdown
    - Description: Convert CSV to Markdown Table
    - Publisher: Marchiore
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Marchiore.csvtomarkdown

1. Markdown Preview Enhanced
    - Id: shd101wyy.markdown-preview-enhanced
    - Description: Markdown Preview Enhanced ported to vscode
    - Publisher: Yiyi Wang
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced
    
    > Edit VS Code's `settings.json` to include the following, this includes some optional settings as examples:

    ```json
    {
      // shd101wyy.markdown-preview-enhanced
      "markdown-preview-enhanced.breakOnSingleNewLine": false,
      "markdown-preview-enhanced.codeBlockTheme": "monokai.css",
      "markdown-preview-enhanced.imageFolderPath": "assets",
      "markdown-preview-enhanced.previewTheme": "one-light.css"
    }
    ```

2. Markdown Table Formatter
    - Id: fcrespo82.markdown-table-formatter
    - Description: A simple markdown plugin to format tables.
    - Publisher: Fernando Crespo
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=fcrespo82.markdown-table-formatter

3. Table Formatter
    - Id: shuworks.vscode-table-formatter
    - Description: Format table syntax of Markdown, Textile and reStructuredText.
    - Publisher: Shuzo Iwasaki
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=shuworks.vscode-table-formatter

### Python

1. Pylance
    - Id: ms-python.vscode-pylance
    - Description: A performant, feature-rich language server for Python in VS Code
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-python.vscode-pylance

    > Edit VS Code's `settings.json` to include the following, this includes some optional settings as examples:

    ```json
    {
      // ms-python.vscode-pylance
      "python.languageServer": "Pylance",
    }
    ```

### Ruby

> To use these extensions, you must first install ruby & rubocop then have it listed in your user path.  

1. Ruby
    - Id: rebornix.ruby
    - Description: Provides Ruby language and debugging support for Visual Studio Code
    - Publisher: Peng Lv
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=rebornix.Ruby

2. ruby-rubocop
    - Id: misogi.ruby-rubocop
    - Description: execute rubocop for current Ruby code.
    - Publisher: misogi
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=misogi.ruby-rubocop

### YAML

1. YAML
    - Id: redhat.vscode-yaml
    - Description: YAML Language Support by Red Hat, with built-in Kubernetes and Kedge syntax support
    - Publisher: Red Hat
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml

1. YAML to JSON
    - Id: ahebrank.yaml2json
    - Description: Convert YAML from clipboard or current document/selection to JSON and vice versa.
    - Publisher: ahebrank
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ahebrank.yaml2json

## Utility Extensions

1. Activitus Bar
    - Id: gruntfuggly.activitusbar
    - Description: Save some real estate by recreating the activity bar buttons on the status bar
    - Publisher: Gruntfuggly
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.activitusbar

    > This post lists how to discover commands: [keybindings](https://code.visualstudio.com/docs/getstarted/keybindings)  
    > This post lists codicon icons: [icons-in-labels](https://code.visualstudio.com/api/references/icons-in-labels)  
    > Edit VS Code's `settings.json` to include the following, this includes some optional settings as examples:

    ```json
    {
      // gruntfuggly.activitusbar
      "workbench.activityBar.visible": false,
      "activitusbar.views": [
        {
          "name": "command.workbench.action.toggleSidebarVisibility",
          "codicon": "output",
          "tooltip": "Toggle Sidebar"
        },
        {
          "name": "explorer",
          "codicon": "files"
        },
        // {
        //   "name": "search",
        //   "codicon": "search"
        // },
        // {
        //   "name": "scm",
        //   "codicon": "source-control"
        // },
        {
          "name": "debug",
          "codicon": "debug-alt"
        },
        {
          "name": "extensions",
          "codicon": "extensions"
        },
        // {
        //   "name": "settings",
        //   "codicon": "gear"
        // },
        // {
        //   "name": "command.workbench.action.reloadWindow",
        //   "codicon": "refresh",
        //   "tooltip": "Reload Window"
        // },
        {
          "name": "command.workbench.action.terminal.toggleTerminal",
          "codicon": "terminal",
          "tooltip": "Terminal (no toggle)"
        },
        {
          "name": "command.workbench.view.extension.gitlens",
          "codicon": "git-merge",
          "tooltip": "Start GitLens View (no toggle)"
        },
        // {
        //   "name": "command.workbench.view.extension.dockerView",
        //   "codicon": "symbol-field",
        //   "tooltip": "Start Docker View (no toggle)"
        // },
        {
          "name": "command.workbench.view.extension.kubernetesView",
          "codicon": "symbol-variable",
          "tooltip": "Start Kubernetes View (no toggle)"
        }
      ]
    }
    ```
            
1. Better Comments
    - Id: aaron-bond.better-comments
    - Description: Improve your code commenting by annotating with alert, informational, TODOs, and more!
    - Publisher: Aaron Bond
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments

    > Edit VS Code's `settings.json` to include the following, this includes some optional settings as examples:

    ```json
    {
      // aaron-bond.better-comments
      "better-comments.tags": [
        {
          "tag": "!:",
          "_comment": "Important Information",
          "color": "#EC465F",
          "strikethrough": false,
          "backgroundColor": "transparent"
        },
        {
          "tag": "ERROR:",
          "_comment": "ERROR Information",
          "color": "#EC465F",
          "strikethrough": false,
          "backgroundColor": "transparent"
        },
        {
          "tag": "?:",
          "_comment": "Explanatory",
          "color": "#AA9DFB",
          "strikethrough": false,
          "backgroundColor": "transparent"
        },
        {
          "tag": "INFO:",
          "_comment": "Explanatory",
          "color": "#AA9DFB",
          "strikethrough": false,
          "backgroundColor": "transparent"
        },
        {
          "tag": "*:",
          "_comment": "Code Block Header/Footer",
          "color": "#FAFF75",
          "strikethrough": false,
          "backgroundColor": "transparent"
        },
        {
          "tag": "DEBUG:",
          "_comment": "Requires Debug/Fix",
          "color": "#F68FFA",
          "strikethrough": false,
          "backgroundColor": "transparent"
        },
        {
          "tag": "NOTE:",
          "_comment": "Extra Information",
          "color": "#92FCAE",
          "strikethrough": false,
          "backgroundColor": "transparent"
        },
        {
          "tag": "REVIEW:",
          "_comment": "Ask for Peer Review",
          "color": "#F69731",
          "strikethrough": false,
          "backgroundColor": "transparent"
        },
        {
          "tag": "TODO:",
          "_comment": "Planned Work",
          "color": "#7AF0FF",
          "strikethrough": false,
          "backgroundColor": "transparent"
        }
      ]
    }
    ```
        
1. Code Runner
    - Id: formulahendry.code-runner
    - Description: Run C, C++, Java, JS, PHP, Python, Perl, Ruby, Go, Lua, Groovy, PowerShell, CMD, BASH, F#, C#, VBScript, TypeScript, CoffeeScript, Scala, Swift, Julia, Crystal, OCaml, R, AppleScript, Elixir, VB.NET, Clojure, Haxe, Obj-C, Rust, Racket, AutoHotkey, AutoIt, Kotlin, Dart, Pascal, Haskell, Nim, D, Lisp
    - Publisher: Jun Han
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner

1. Code Spell Checker
    - Id: streetsidesoftware.code-spell-checker
    - Description: Spelling checker for source code
    - Publisher: Street Side Software
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker
    
    > Edit VS Code's `settings.json` to include the following, this includes some optional settings as examples:

    ```json
    {
      // streetsidesoftware.code-spell-checker
      "cSpell.userWords": [
        "Prepend",
        "dotnetcore",
        "preconfigured"
      ],
      "cSpell.enableFiletypes": [
        "org",
        "plaintext"
      ]
    }
    ```

1. GitLens — Git supercharged
    - Id: eamodio.gitlens
    - Description: Supercharge the Git capabilities built into Visual Studio Code — Visualize code authorship at a glance via Git blame annotations and code lens, seamlessly navigate and explore Git repositories, gain valuable insights via powerful comparison commands, and so much more
    - Publisher: Eric Amodio
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens

1. Live Share
    - Id: ms-vsliveshare.vsliveshare
    - Description: Real-time collaborative development from the comfort of your favorite tools.
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare

1. Live Share Extension Pack
    - Id: ms-vsliveshare.vsliveshare-pack
    - Description: Collection of extensions that enable real-time collaborative development with VS Live Share.
    - Publisher: Microsoft
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare-pack

1. Rainbow CSV
    - Id: mechatroner.rainbow-csv
    - Description: Highlight CSV and TSV files in different colors, Run SQL-like queries
    - Publisher: mechatroner
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=mechatroner.rainbow-csv

1. Select Line Status Bar
    - Id: tomoki1207.selectline-statusbar
    - Description: Displays selected lines count in status bar
    - Publisher: tomoki1207
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=tomoki1207.selectline-statusbar

1. Syncify
    - Id: arnohovhannisyan.syncify
    - Description: A reliable way of syncing your VSCode settings and extensions
    - Publisher: Arno Hovhannisyan
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=arnohovhannisyan.syncify

## Theme Extensions

### Color Theme

1. ninja-ui-vibrant
    - Id: ntkvu.ninja-ui-vibrant
    - Description: Ninja UI Vibrant Syntax Theme
    - Publisher: ntkvu
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ntkvu.ninja-ui-vibrant

    > Edit VS Code's `settings.json` to include the following, this includes some optional settings as examples:

    ```json
    {
      // ntkvu.ninja-ui-vibrant
      "workbench.colorCustomizations": {
        "checkbox.background": "#111B22",
        "textBlockQuote.border": "#D8CECA",
        "textLink.foreground": "#7AF0FF",
        "textPreformat.foreground": "#92FCAE"
      }
    }
    ```
        
1. Dracula Official
    - Id: dracula-theme.theme-dracula
    - Description: Official Dracula Theme. A dark theme for many editors, shells, and more.
    - Publisher: Dracula Theme
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=dracula-theme.theme-dracula

### File Icon Theme

1. file-icons
    - Id: file-icons.file-icons
    - Description: File-specific icons in VSCode for improved visual grepping.
    - Publisher: file-icons
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=file-icons.file-icons

1. Material Icon Theme
    - Id: pkief.material-icon-theme
    - Description: Material Design Icons for Visual Studio Code
    - Publisher: Philipp Kief
    - VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme

# Recommended Settings

Edit VS Code's `settings.json` to include the following:

```json
{
  // General Settings
  "workbench.startupEditor": "newUntitledFile",
  "files.eol": "\n",
  "window.zoomLevel": 0,
  "editor.detectIndentation": false,
  "editor.tabSize": 2,
  "editor.renderWhitespace": "all",
  "files.autoSave": "afterDelay",
  // Theming
  "workbench.iconTheme": "file-icons",
  "workbench.colorTheme": "Ninja Ui Vibrant",
  "workbench.preferredDarkColorTheme": "Ninja Ui Vibrant",
  // ntkvu.ninja-ui-vibrant
  "workbench.colorCustomizations": {
    "checkbox.background": "#111B22",
    "textBlockQuote.border": "#D8CECA",
    "textLink.foreground": "#7AF0FF",
    "textPreformat.foreground": "#92FCAE"
}
```

# Tom's VS Code Syncify Repo

https://github.com/tbuico/vs-code

[Official docs](https://arnohovhannisyan.space/vscode-syncify/) 

1. Setup Syncify on computer *A*
2. Init a new public Syncify repo in github using oauth
3. On computer *A*, run `Syncify: Upload`
4. Verify that the settings uploaded in to the new repo in github
5. Setup Syncify on computer *B* with github oauth
6. Chose the existing public Syncify repo in github in vscode
7. On computer *B*, run `Syncify: Download`